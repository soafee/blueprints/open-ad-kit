# Autoware Open AD Kit Blueprint R2

## Overview

This blueprint represents an iteration of the original [R1 blueprint](../r1/README.md).  The majority of the functional containers will be kept the same as the R1 blueprint, with the exception of the actuation container.

The goal of this blueprint is to understand what is needed to enable true heterogeneous compute across application and realtime processors in a deployed solution.  At a high level, this will required understanding:

- The relationship with realtime processors (Cortex-R) and OCI containers
- How to test non-application processor code (Cortex-A) in the cloud
- How to define and validate in a CI/CD pipeline the realtime characteristics of the workload
- The relationship between the in-vehicle orchestrator and the deployed heterogeneous workload
- ... More will emerge as we explore the solution space ...

## PoC Implementation

There is an initial proof of concept implementation of the blueprint that showcases a functional end result.  This was implemented through collaboration with SOAFEE partners Kernkonzept, NXP and Arm.  The code for this implementation is in the process of being upstreamed and we will have details of where to find it when available.

The PoC took the actuation function from the R1 Open AD Kit blueprint and re-implemented it on top of an RTOS (Zephyr) and deployed it to an NXP S32Z (Cortex-R52) running with locked cores to aid with the safety story.

However, the PoC is a hand crafted solution to prove a specific technical requirement, there was no consideration for how you would put this workflow into production.

## Open tickets

As the breakdown of the requirements of this blueprint progress, top level tickets will be created to capture the asks and track progress towards a functional implementation.

- &5+

## Contribution

Please feel free to engage in the progression of the tickets towards the goals of this blueprint.  The solution will require expertise from a broad skill base, and we need your input to reach the correct solution.